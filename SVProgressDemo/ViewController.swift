//
//  ViewController.swift
//  SVProgressDemo
//
//  Created by Edy Cu Tjong on 6/11/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import SVProgressHUD

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        SVProgressHUD.show(withStatus: "It's working...")
    }
}

